package net.thumbtack.ogorodnikov.trainee;

import static org.junit.Assert.*;

import org.junit.Test;

import net.thumbtack.ogorodnikov.trainee.lesson1.TriangleTrainee;
import net.thumbtack.ogorodnikov.trainee.lesson1.figure.Point;
import net.thumbtack.ogorodnikov.trainee.lesson1.figure.Point2D;

public class TriangleTraineeTest {

	private TriangleTrainee triangle;
	
	@Test
	public void testCorrectTriangleMoving() throws Exception{
		triangle = new TriangleTrainee(0, 0, 2, 1, 4, 0);
		
		triangle.MoveTriangle(1, 2);
		
		Point[] points = new Point[3];
		
		points[0] = new Point(1, 2);
		points[1] = new Point(3, 3);
		points[2] = new Point(5, 2);
		
		assertEquals(points, triangle.GetPoints());	
	}
	
	@Test
	public void testCalculateSquareTriangle() throws Exception{
		triangle = new TriangleTrainee(0, 0, 2, 1, 4, 0);
		
		assertEquals(triangle.CalculateSquare(), 2.0, 0.001);
	}
	
	@Test
	public void testInsidePointTriangle() throws Exception{
		triangle = new TriangleTrainee(0, 0, 0, 4, 4, 0);
		
		assertTrue(triangle.InsidePoint(1, 1));
	}
	
	@Test
	public void testNotInsidePointTriangle() throws Exception{
		triangle = new TriangleTrainee(0, 0, 0, 4, 4, 0);
		
		assertFalse(triangle.InsidePoint(5, 1));
	}
	
	@Test
	public void testInsidePoint2DTriangle() throws Exception{
		triangle = new TriangleTrainee(0, 0, 0, 4, 4, 0);
		
		assertTrue(triangle.InsidePoint2D(new Point2D(1, 1)));
	}
	
	@Test
	public void testNotInsidePoint2DTriangle() throws Exception{
		triangle = new TriangleTrainee(0, 0, 0, 4, 4, 0);
		
		assertFalse(triangle.InsidePoint2D(new Point2D(5, 1)));
	}

	@Test
	public void testEqualSidesTriangle() throws Exception{
		triangle = new TriangleTrainee(0, 0, 3, Math.sqrt(27), 6, 0);
		
		assertTrue(triangle.EqualsSides());
	}
	
	@Test
	public void testNotEqualSidesTriangle() throws Exception{
		triangle = new TriangleTrainee(0, 0, 0, 4, 4, 0);
		
		assertFalse(triangle.EqualsSides());
	}

}
