package net.thumbtack.ogorodnikov.trainee;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;
import net.thumbtack.ogorodnikov.trainee.lesson1.LinkedListTrainee;
import net.thumbtack.ogorodnikov.trainee.lesson1.figure.NodeTrainee;

public class LinkedListTraineeTest {

	private LinkedListTrainee list;
	private LinkedListTrainee other;
	
	@Before
	public void CreateList() {
		list = new LinkedListTrainee();
		other = new LinkedListTrainee();
	}
	
	@Test
	public void testCorrectFormsHeadElement() throws Exception{
		list.add(3);
		list.add(4);
		list.add(5);
		
		NodeTrainee node = list.getHead();
		
		assertEquals(node.getData(), 3);
	}

	@Test
	public void testCorrectFormsLastElement() throws Exception{
		list.add(3);
		list.add(4);
		list.add(5);
		
		NodeTrainee node = list.getLast();
		
		assertEquals(node.getData(), 5);
	}
	
	@Test
	public void testCorrentGetInit() throws Exception{
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		
		other.add(1);
		other.add(2);
		other.add(3);
		other.add(4);
	
		assertEquals(other, list.GetInit());
	}
	
	@Test
	public void testCorrectGetInitEmptyList() throws Exception{
		list.add(1);
		
		assertEquals(other, list.GetInit());
	}
	
	@Test
	public void testCorrectGetTail() throws Exception{
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		
		other.add(2);
		other.add(3);
		other.add(4);
		other.add(5);
	
		assertEquals(other, list.GetTail());
	}
	
	@Test
	public void testCorrectGetTailEmptyList() throws Exception{
		list.add(1);
		
		assertEquals(other, list.GetTail());
	}
	
	@Test
	public void testListsEqualSameOrder() throws Exception {
		list.add(1);
		list.add(2);
		list.add(3);
		
		other.add(1);
		other.add(2);
		other.add(3);
		
		assertTrue(list.equals(other));
	}
	
	@Test
	public void testListsEqualBothNull() throws Exception {
		assertTrue(list.equals(other));
	}
	
	@Test
	public void testListsNotEqualDifferentOrderElements() throws Exception {
		list.add(3);
		list.add(2);
		list.add(1);
		
		other.add(1);
		other.add(2);
		other.add(3);
		
		assertFalse(list.equals(other));
	}
	
	@Test
	public void testListsNotEqualDifferentSizes() throws Exception {
		list.add(1);
		list.add(2);
		list.add(3);
		
		other.add(1);
		other.add(2);
		
		assertFalse(list.equals(other));
	}
	
	@Test
	public void testListsNotEqualOneNull() throws Exception{
		list.add(1);
		
		assertFalse(list.equals(other));
	}
}
