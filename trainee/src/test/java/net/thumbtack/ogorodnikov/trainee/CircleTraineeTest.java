package net.thumbtack.ogorodnikov.trainee;

import static org.junit.Assert.*;

import org.junit.Test;

import net.thumbtack.ogorodnikov.trainee.lesson1.CircleTrainee;
import net.thumbtack.ogorodnikov.trainee.lesson1.figure.Point2D;

public class CircleTraineeTest {

	private CircleTrainee circle;
	
	@Test
	public void testInsidePointCircle() throws Exception{
		circle = new CircleTrainee(0, 0, 3);
		
		assertTrue(circle.InsidePoint(1, 1));
	}

	@Test
	public void testNotInsidePointCircle() throws Exception{
		circle = new CircleTrainee(0, 0, 3);
		
		assertTrue(circle.InsidePoint(-1, 1));
	}
	
	@Test
	public void testInsidePoint2DCircle() throws Exception{
		circle = new CircleTrainee(0, 0, 3);
		
		assertTrue(circle.InsidePoint2D(new Point2D(1, 1)));
	}

	@Test
	public void testNotInsidePoint2DCircle() throws Exception{
		circle = new CircleTrainee(0, 0, 3);
		
		assertTrue(circle.InsidePoint2D(new Point2D(-1, 1)));
	}
	
	@Test
	public void testCalculateSquareCircle() throws Exception{
		circle = new CircleTrainee(0, 0, 3);
		
		assertEquals(circle.CalculateSquare(), 28.274, 0.001);
	}
	
	@Test
	public void testCalculateLengthCircle() throws Exception{
		circle = new CircleTrainee(0, 0, 3);
		
		assertEquals(circle.CalculateLength(), 18.849, 0.001);
	}
}
