package net.thumbtack.ogorodnikov.trainee;

import static org.junit.Assert.*;

import org.junit.Test;

import net.thumbtack.ogorodnikov.trainee.lesson1.RectangleTrainee;
import net.thumbtack.ogorodnikov.trainee.lesson1.figure.*;

public class RectangleTraineeTest {

	private RectangleTrainee rectangle;
	
	@Test
	public void testCorrectMoveRectangle() throws Exception {
		rectangle = new RectangleTrainee(0, 0, 0, 3, 4, 3, 4, 0);
		
		rectangle.MoveRectangle(3, -4);
		
		Point[] points = rectangle.GetPoints();
		
		Point[] pointsAfterMoving = new Point[4];
		pointsAfterMoving[0] = new Point(3, -4);
		pointsAfterMoving[1] = new Point(3, -1);
		pointsAfterMoving[2] = new Point(7, -1);
		pointsAfterMoving[3] = new Point(7, -4);
		
		assertEquals(points,  pointsAfterMoving);
	}
	
	@Test
	public void testCorrectMoveZeroPointRectanle() throws Exception {
		rectangle = new RectangleTrainee(0, 0, 0, 3, 4, 3, 4, 0);
		
		rectangle.MoveRectangle(0, 0);
		
		Point[] points = rectangle.GetPoints();
		
		Point[] pointsAfterMoving = new Point[4];
		pointsAfterMoving[0] = new Point(0, 0);
		pointsAfterMoving[1] = new Point(0, 3);
		pointsAfterMoving[2] = new Point(4, 3);
		pointsAfterMoving[3] = new Point(4, 0);
		
		assertEquals(points,  pointsAfterMoving);
	}
	
	@Test
	public void testCorrectResizeRectangle() throws Exception {
		rectangle = new RectangleTrainee(0, 0, 0, 4, 4, 4, 4, 0);
		
		rectangle.ResizeRectangle(2, 2);
		
		Point[] points = rectangle.GetPoints();
		
		Point[] pointsAfterMoving = new Point[4];
		pointsAfterMoving[0] = new Point(0, 0);
		pointsAfterMoving[1] = new Point(0, 2);
		pointsAfterMoving[2] = new Point(2, 2);
		pointsAfterMoving[3] = new Point(2, 0);
		
		assertEquals(points,  pointsAfterMoving);
	}
	
	@Test
	public void testNullResizeRectangle() throws ArithmeticException {
		rectangle = new RectangleTrainee(0, 0, 0, 4, 4, 4, 4, 0);
		
		try{
			rectangle.ResizeRectangle(0, 0);
		}
		catch(ArithmeticException e){
			assertEquals(e.getMessage(), "Error in input data: parameter(s) are zero");
		}
	}
	
	@Test
	public void testNegativeResizeRectangle() throws ArithmeticException {
		rectangle = new RectangleTrainee(0, 0, 0, 4, 4, 4, 4, 0);
		
		try{
			rectangle.ResizeRectangle(-2, -2);
		}
		catch(ArithmeticException e){
			assertEquals(e.getMessage(), "Error in input data: parameter(s) are negative");
		}
	}
	
	@Test
	public void testCalculateSquarePositiveWholeCoordinatesRectangle() throws Exception {
		rectangle = new RectangleTrainee(0, 0, 0, 4, 4, 4, 4, 0);
		
		double square = rectangle.CalculateSquare();
		
		assertEquals(square, 16.0, 0.001);
	}
	
	@Test
	public void testCalculateSquareNegativeWholeCoordinatesRectangle() throws Exception {
		rectangle = new RectangleTrainee(0, 0, 0, -4, -4, -4, -4, 0);
		
		double square = rectangle.CalculateSquare();
		
		assertEquals(square, 16.0, 0.001);
	}

	@Test
	public void testCalculateSquareMixedWholeCoordinatesRectangle() throws Exception {
		rectangle = new RectangleTrainee(0, 0, 0, 4, 4, -4, -4, 0);
		
		double square = rectangle.CalculateSquare();
		
		assertEquals(square, 16.0, 0.001);
	}
	
	@Test
	public void testCalculateSquareDoubleCoordinatesRectangle() throws Exception {
		rectangle = new RectangleTrainee(0.5, 0.5, 0.5, 4.5, 4, -4, 4.5, 0);
		
		double square = rectangle.CalculateSquare();
		
		assertEquals(square, 16.1245, 0.001);
	}
	
	@Test
	public void testInsidePointRectangle() throws Exception {
		rectangle = new RectangleTrainee(0, 0, 0, 4, 4, 4, 4, 0);
		
		assertTrue(rectangle.InsidePoint(1, 1));
	}
	
	@Test
	public void testNotInsidePointRectangle() throws Exception {
		rectangle = new RectangleTrainee(0, 0, 0, 4, 4, -4, -4, 0);
		
		assertFalse(rectangle.InsidePoint(1, 1));
	}
	
	@Test
	public void testCheckPoint2DPlaceRectangle() throws Exception {
		rectangle = new RectangleTrainee(0, 0, 0, 4, 4, 4, 4, 0);
		
		Point2D point2d = new Point2D(1, 1);
		
		assertTrue(rectangle.InsidePoint2D(point2d));
	}
	
	@Test
	public void testNotCheck2DPointPlaceRectangle() throws Exception {
		rectangle = new RectangleTrainee(0, 0, 0, 4, 4, -4, -4, 0);
		
		Point2D point2d = new Point2D(1, 1);
		
		assertFalse(rectangle.InsidePoint2D(point2d));
	}
	
	@Test
	public void testCorrectContainsParallelCoordinatesRectangle() throws Exception {
		rectangle = new RectangleTrainee(0, 0, 0, 8, 8, 8, 8, 0);
		
		RectangleTrainee rectangleOther = new RectangleTrainee(1, 1, 1, 7, 7, 7, 7, 1);
		
		assertTrue(rectangle.Contains(rectangleOther));
	}
	
	@Test
	public void testCorrectContainsNotParallelCoordinatesRectangle() throws Exception {
		rectangle = new RectangleTrainee(9, 2, 11, 4, 14, 1, 12, -2);
		
		RectangleTrainee rectangleOther = new RectangleTrainee(10, 2, 11, 3, 12, -1, 13, 1);
		
		assertTrue(rectangle.Contains(rectangleOther));
	}
	
	@Test
	public void testCorrectNotContainsNotParallelCoordinatesRectangle() throws Exception {
		rectangle = new RectangleTrainee(1, 3, 3, 5, 3,-1, 5, 1);
		
		RectangleTrainee rectangleOther = new RectangleTrainee(3, 2, 5, 4, 7, 2, 5, -1);
		
		assertFalse(rectangle.Contains(rectangleOther));
	}
	
	@Test
	public void testContainsNotIntersectionRectangle() throws Exception {
		rectangle = new RectangleTrainee(0, 0, 0, 8, 8, 8, 8, 0);
		
		RectangleTrainee rectangleOther = new RectangleTrainee(1, 1, 1, 7, 7, 7, 7, 1);
		
		assertFalse(rectangle.Intersection(rectangleOther));
	}
	
	@Test
	public void testIntersectionParallelCoordinatesRectangle() throws Exception {
		rectangle = new RectangleTrainee(0, 0, 0, 8, 8, 8, 8, 0);
		
		RectangleTrainee rectangleOther = new RectangleTrainee(1, 1, 1, 9, 9, 9, 9, 1);
		
		assertTrue(rectangle.Intersection(rectangleOther));
	}
	
	@Test
	public void testIntersectionNotParallelCoordinatesRectangle() throws Exception {
		rectangle = new RectangleTrainee(1, 3, 3, 5, 5, 1, 3, -1);
		
		RectangleTrainee rectangleOther = new RectangleTrainee(3, 2, 5, 4, 7, 2, 5, -1);
		
		assertTrue(rectangle.Intersection(rectangleOther));
	}
	
	@Test
	public void testNotIntersectionNotParallelCoordinatesRectangle() throws Exception {
		rectangle = new RectangleTrainee(1, 3, 3, 5, 5, 1, 3, -1);
		
		RectangleTrainee rectangleOther = new RectangleTrainee(10, 2, 11, 3, 12, -1, 13, 1);
		
		assertFalse(rectangle.Intersection(rectangleOther));
	}
	
	@Test
	public void testCorrectLargeTwoSizeRectangle() throws Exception {
		rectangle = new RectangleTrainee(0, 0, 0, 8, 8, 8, 8, 0);
		
		rectangle.Large(2);
		
		Point[] pointsLarge = new Point[4];
		
		pointsLarge[0] = new Point(-4, -4);
		pointsLarge[1] = new Point(-4, 12);
		pointsLarge[2] = new Point(12, 12);
		pointsLarge[3] = new Point(12, -4);
		
		assertEquals(rectangle.GetPoints(), pointsLarge);
	}
	
	@Test
	public void testCorrectLargeSameSizeRectangle() throws Exception {
		rectangle = new RectangleTrainee(0, 0, 0, 8, 8, 8, 8, 0);
		
		rectangle.Large(1);
		
		Point[] pointsLarge = new Point[4];
		
		pointsLarge[0] = new Point(0, 0);
		pointsLarge[1] = new Point(0, 8);
		pointsLarge[2] = new Point(8, 8);
		pointsLarge[3] = new Point(8, 0);
		
		assertEquals(rectangle.GetPoints(), pointsLarge);
	}
	
	@Test
	public void testNullLargeRectangle() throws ArithmeticException {
		rectangle = new RectangleTrainee(0, 0, 0, 4, 4, 4, 4, 0);
		
		try{
			rectangle.Large(0);
		}
		catch(ArithmeticException e){
			assertEquals(e.getMessage(), "Error in input data: parameter is zero");
		}
	}
	
	@Test
	public void testNegativeLargeRectangle() throws ArithmeticException {
		rectangle = new RectangleTrainee(0, 0, 0, 4, 4, 4, 4, 0);
		
		try{
			rectangle.Large(-1);
		}
		catch(ArithmeticException e){
			assertEquals(e.getMessage(), "Error in input data: parameter is negative");
		}
	}
}
