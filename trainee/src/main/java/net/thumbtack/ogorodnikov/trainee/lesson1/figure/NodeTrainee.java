package net.thumbtack.ogorodnikov.trainee.lesson1.figure;

public class NodeTrainee {

	private int data;
	private NodeTrainee next;
	private NodeTrainee prev;
	
	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
	}
	
	public void setNext(NodeTrainee node){
		this.next = node;
	}
	
	public NodeTrainee getNext(){
		return next;
	}
	
	public void setPrev(NodeTrainee node){
		this.prev = node;
	}
	
	public NodeTrainee getPrev(){
		return prev;
	}
	
	public NodeTrainee(NodeTrainee prev, NodeTrainee next, int data){
		this.data = data;
		this.prev = prev;
		this.next = next;
	}
}
