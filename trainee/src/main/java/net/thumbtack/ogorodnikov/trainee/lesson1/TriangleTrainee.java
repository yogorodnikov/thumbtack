package net.thumbtack.ogorodnikov.trainee.lesson1;
import net.thumbtack.ogorodnikov.trainee.lesson1.figure.*;

public class TriangleTrainee extends Figure{

	public TriangleTrainee(double x1, double y1, double x2, double y2, double x3, double y3) {		
		super(x1, y1, x2, y2, x3, y3);
	}

	public void MoveTriangle(double dx, double dy) {
		for (int i = 0; i < 3; ++i) {
			ChangeCoordinates(i, dx, dy);
		}
	}

	public double CalculateSquare() {
		Point[] points = this.GetPoints();
		Point point1 = points[0];
		Point point2 = points[1];
		Point point3 = points[2];
		
		double length12 = CalculateLength(point1.getX(), point1.getY(), point2.getX(), point2.getY());
		double length13 = CalculateLength(point1.getX(), point1.getY(), point3.getX(), point3.getY());
		double length23 = CalculateLength(point2.getX(), point2.getY(), point3.getX(), point3.getY());

		double p = (length12 + length13 + length23) / 2;

		return Math.sqrt(p * (p - length12) * (p - length13) * (p - length23));
	}

	public boolean InsidePoint(double x0, double y0) {
		Point[] points = this.GetPoints();
		Point point1 = points[0];
		Point point2 = points[1];
		Point point3 = points[2];
		
		double r1 = (point2.getY() - point1.getY()) * (x0 - point1.getX()) - (y0 - point1.getY()) * (point2.getX() - point1.getX());
		double r2 = (point3.getY() - point2.getY()) * (x0 - point2.getX()) - (y0 - point2.getY()) * (point3.getX() - point2.getX()); 
		double r3 = (point1.getY() - point3.getY()) * (x0 - point3.getX()) - (y0 - point3.getY()) * (point1.getX() - point3.getX());
		
		return ((r1 > 0 && r2 > 0 && r3 > 0) || (r1 < 0 && r2 < 0 && r3 < 0));
	}

	public boolean InsidePoint2D(Point2D other) {
		Point[] points = this.GetPoints();
		Point point1 = points[0];
		Point point2 = points[1];
		Point point3 = points[2];
		
		double r1 = (point2.getY() - point1.getY()) * (other.getX() - point1.getX()) - (other.getY() - point1.getY()) * (point2.getX() - point1.getX());
		double r2 = (point3.getY() - point2.getY()) * (other.getX() - point2.getX()) - (other.getY() - point2.getY()) * (point3.getX() - point2.getX()); 
		double r3 = (point1.getY() - point3.getY()) * (other.getX() - point3.getX()) - (other.getY() - point3.getY()) * (point1.getX() - point3.getX());

		return ((r1 > 0 && r2 > 0 && r3 > 0) || (r1 < 0 && r2 < 0 && r3 < 0));
	}

	public boolean EqualsSides() {
		Point[] points = this.GetPoints();
		Point point1 = points[0];
		Point point2 = points[1];
		Point point3 = points[2];
		
		double s12 = CalculateLength(point1.getX(), point1.getY(), point2.getX(), point2.getY());
		double s13 = CalculateLength(point1.getX(), point1.getY(), point3.getX(), point3.getY());
		double s23 = CalculateLength(point2.getX(), point2.getY(), point3.getX(), point3.getY());

		return (s12 == s13 && s13 == s23);
	}

	private double CalculateLength(double x1, double y1, double x2, double y2) {
		return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
	}
}
