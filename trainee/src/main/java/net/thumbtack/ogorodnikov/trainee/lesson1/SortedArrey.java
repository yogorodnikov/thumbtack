package net.thumbtack.ogorodnikov.trainee.lesson1;

import java.util.Scanner;

public class SortedArrey {
	
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		
		int[] array = new int[N];
		
		for (int i = 0; i < N; ++i)
		{
			array[i] = in.nextInt();
		}
		
	boolean is_increased = true;
	boolean is_decreased = true;
	
	for (int i = 0; i < N - 1; ++i)
	{
		if (array[i] >= array[i + 1])
		{
			is_increased = false;
		}
		
		if (array[i] <= array[i + 1])
		{
			is_decreased = false;
		}
	}
	
	if (is_increased)
		System.out.println("The array is increased");
	else
		System.out.println("The array isn't increased");
	
	if (is_decreased)
		System.out.println("The array is decreased");
	else
		System.out.println("The array isn't decreased");
	
	in.close();
	}
}
