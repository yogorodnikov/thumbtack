package net.thumbtack.ogorodnikov.trainee.lesson1;

import java.util.Scanner;

public class DoubleNumbers {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		double a = in.nextDouble();
		double b = in.nextDouble();

		System.out.println("Product: " + (a * b));
		System.out.println("Sum: " + (a + b));

		if (a < b)
			System.out.println("a less than b");
		else if (a > b)
			System.out.println("b less than a");
		else
			System.out.println("a is equals b");

		in.close();
	}
}
