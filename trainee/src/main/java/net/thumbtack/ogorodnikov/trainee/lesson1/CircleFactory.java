package net.thumbtack.ogorodnikov.trainee.lesson1;

public class CircleFactory {

	private static int instances;

	static {
		instances = 0;
	}

	public static CircleTrainee createCircle(double x, double y, double r) {
		instances++;
		return new CircleTrainee(x, y, r);
	}

	public int getInstances() {
		return instances;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
