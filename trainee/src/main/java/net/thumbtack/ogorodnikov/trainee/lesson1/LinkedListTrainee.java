package net.thumbtack.ogorodnikov.trainee.lesson1;

import net.thumbtack.ogorodnikov.trainee.lesson1.figure.NodeTrainee;

public class LinkedListTrainee {
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((head == null) ? 0 : head.hashCode());
		result = prime * result + ((last == null) ? 0 : last.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LinkedListTrainee other = (LinkedListTrainee) obj;
		if (head == null) {
			if (other.head != null)
				return false;
		}
		
		if (last == null) {
			if (other.last != null)
				return false;
		} 
		
		if (this.GetSize() != other.GetSize()){
			return false;
		}
		
		NodeTrainee node = head;
		NodeTrainee nodeOther = other.head;
		while (node != null && nodeOther != null){
			if (node.getData() != nodeOther.getData()){
				return false;
			}
			node = node.getNext();
			nodeOther = nodeOther.getNext();
		}
		return true;
	}

	private NodeTrainee head;
	private NodeTrainee last;
	
	public LinkedListTrainee(){
		head = null;
		last = null;
	}
	
	public void add(int data){
		NodeTrainee node = new NodeTrainee(null, null, data);
		if (head == null){
			head = node;
		}
		
		if (last == null){
			last = node;
		} else {
			last.setNext(node);
			node.setPrev(last);
			last = node;
		}
	}
	
	public NodeTrainee getHead(){
		return head;
	}
	
	public NodeTrainee getLast(){
		return last;
	}
	
	public int GetSize(){
		int size = 0;
		
		NodeTrainee node = head;
		
		while (node != null){
			node = node.getNext();
			size++;
		}
		
		return size;
	}
	
	public LinkedListTrainee GetInit(){
		LinkedListTrainee newList = new LinkedListTrainee();
		NodeTrainee iterationNode = head;
		
		while (iterationNode.getNext() != null){
			newList.add(iterationNode.getData());
			iterationNode = iterationNode.getNext();
		}
		
		return newList;
	}
	
	public LinkedListTrainee GetTail(){
		LinkedListTrainee newList = new LinkedListTrainee();
		NodeTrainee iterationNode = head.getNext();
		
		while (iterationNode != null){
			newList.add(iterationNode.getData());
			iterationNode = iterationNode.getNext();
		}
		
		return newList;
	}
	
	public static void main(String[] args){
		LinkedListTrainee list = new LinkedListTrainee();
		list.add(3);
		LinkedListTrainee listTest = list.GetInit();
		
		LinkedListTrainee list2 = new LinkedListTrainee();
		listTest = list.GetInit();
		
		if (list2.equals(listTest)){
			System.out.println("Equals");
		}
		else{
			System.out.println("Not equals");
		}
	}
}


