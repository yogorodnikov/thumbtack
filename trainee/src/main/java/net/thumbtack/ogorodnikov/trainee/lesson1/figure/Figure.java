package net.thumbtack.ogorodnikov.trainee.lesson1.figure;

import java.util.Arrays;

public abstract class Figure {
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(points);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Figure other = (Figure) obj;
		if (!Arrays.equals(points, other.points))
			return false;
		return true;
	}

	private Point[] points;
	
		
	//old version - only for RectangleTrainee
	public Figure(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4) {
		points = new Point[4];
		points[0] = new Point(x1, y1);
		points[1] = new Point(x2, y2);
		points[2] = new Point(x3, y3);
		points[3] = new Point(x4, y4);
	}
	
	//it's very bad
	public Figure(double x1, double y1, double x2, double y2, double x3, double y3) {
		points = new Point[3];
		points[0] = new Point(x1, y1);
		points[1] = new Point(x2, y2);
		points[2] = new Point(x3, y3);
	}
	
	public Figure(double width, double height){
		points = this.GetPoints();
		points = new Point[4];

		points[0] = new Point(0, 0);
		points[1] = new Point(0, height);
		points[2] = new Point(width, 0);
		points[3] = new Point(width, height);
	}
	
	
	public abstract double CalculateSquare();

	public void PrintPoints(){
		for (Point el : points){
			System.out.print("Point: " + el.getX() + " " + el.getY());
		}
	}
	
	public void SetPoints(Point[] points){
		for (int i = 0; i < points.length; ++i){
			this.points[i].setX(points[i].getX());
			this.points[i].setY(points[i].getY());
		}
	}
	
	public Point[] GetPoints(){
		return points;
	}
	
	public void ChangeCoordinates(int number, double x, double y){
		if (number >= points.length){
			System.err.println("Here must be out of range exception");
			return;
		}
		
		points[number].setX(points[number].getX() + x);
		points[number].setY(points[number].getY() + y);
	}
	
	public abstract boolean InsidePoint(double x, double y);
	public abstract boolean InsidePoint2D(Point2D point);
}
