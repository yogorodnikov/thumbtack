package net.thumbtack.ogorodnikov.trainee.lesson1;

import java.util.Scanner;

public class Rectangle {
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		int x_1 = in.nextInt();
		int y_1 = in.nextInt();
		
		int x_2 = in.nextInt();
		int y_2 = in.nextInt();
		
		int x_point = in.nextInt();
		int y_point = in.nextInt();
		
		if (x_point >= x_1 && x_point <= x_2 && y_point >= y_1 && y_point <= y_2)
			System.out.println("Inside");
		else
			System.out.println("Outside");
		
		in.close();
	}
}
