package net.thumbtack.ogorodnikov.trainee.lesson1;
import net.thumbtack.ogorodnikov.trainee.lesson1.figure.*;

public class CircleTrainee {

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(r);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CircleTrainee other = (CircleTrainee) obj;
		if (Double.doubleToLongBits(r) != Double.doubleToLongBits(other.r))
			return false;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}

	private double x;
	private double y;
	private double r;

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
	}

	public CircleTrainee(double x, double y, double r) {
		this.x = x;
		this.y = y;
		this.r = r;
	}

	public void MoveCircle(double dx, double dy) {
		this.x += dx;
		this.y += dy;
	}

	public void PrintCenterRadius() {
		System.out.println("Center: x" + this.x);
		System.out.println("Center: y" + this.y);
		System.out.println("Radius " + this.r);
	}

	public boolean InsidePoint(double x0, double y0) {
		return ((x - x0) * (x - x0) + (y - y0) * (y - y0) < r * r);
	}

	public boolean InsidePoint2D(Point2D other) {
		return ((x - other.getY()) * (x - other.getX()) + (y - other.getY()) * (y - other.getY()) < r * r);
	}

	public double CalculateSquare() {
		return Math.PI * r * r;
	}

	public double CalculateLength() {
		return 2 * Math.PI * r;
	}

}
