package net.thumbtack.ogorodnikov.trainee.lesson1;

import java.util.Scanner;

public class ArrayN {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();

		int[] array = new int[N];

		for (int i = 0; i < N; ++i) {
			array[i] = in.nextInt();
		}

		int sum = 0, prod = 1, min = 100500, max = -100500;

		double mean = 0;

		for (int i = 0; i < N; ++i) {
			sum += array[i];
			prod *= array[i];
			if (array[i] < min)
				min = array[i];

			if (array[i] > max)
				max = array[i];

			mean += array[i];
		}

		System.out.println("Sum: " + sum);
		System.out.println("Product: " + prod);
		System.out.println("Min: " + min);
		System.out.println("Max: " + max);
		System.out.println("Mean: " + (double) (mean / N));
		
		in.close();
	}
}
