package net.thumbtack.ogorodnikov.trainee.lesson1.figure;

public class Point2D {
	private double x;
	private double y;

	public Point2D(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	public void moveTo(int newX, int newY) {
		x = newX;
		y = newY;
	}
}
