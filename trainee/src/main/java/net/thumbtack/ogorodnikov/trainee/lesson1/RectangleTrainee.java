package net.thumbtack.ogorodnikov.trainee.lesson1;

import net.thumbtack.ogorodnikov.trainee.lesson1.figure.*;

public class RectangleTrainee extends Figure {

	public RectangleTrainee(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4) {
		super(x1, y1, x2, y2, x3, y3, x4, y4);
	}

	public RectangleTrainee(double width, double height) {
		super(width, height);
	}

	public RectangleTrainee() {
		this(1, 1);
	}

	public void PrintVertexes() {
		this.PrintVertexes();
	}

	public void MoveRectangle(double dx, double dy) {
		for (int i = 0; i < 4; ++i) {
			ChangeCoordinates(i, dx, dy);
		}
	}

	public void ResizeRectangle(double nx, double ny) throws ArithmeticException {

		if (nx == 0 || ny == 0) {
			throw new ArithmeticException("Error in input data: parameter(s) are zero");
		}
		
		if (nx < 0 || ny < 0) {
			throw new ArithmeticException("Error in input data: parameter(s) are negative");
		}

		Point[] points = this.GetPoints();

		for (int i = 1; i < 4; ++i) {
			Point point = points[i];
			point.setX(point.getX() / nx);
			point.setY(point.getY() / ny);
		}

	}

	public double CalculateSquare() {
		Point[] points = this.GetPoints();
		Point point1 = points[0];
		Point point2 = points[1];
		Point point4 = points[3];

		double length_1 = this.CalculateLength(point1.getX(), point1.getY(), point2.getX(), point2.getY());
		double length_2 = this.CalculateLength(point1.getX(), point1.getY(), point4.getX(), point4.getY());

		return length_1 * length_2;
	}

	public boolean InsidePoint(double x, double y) {
		Point[] points = this.GetPoints();
		return (x >= points[0].getX() && x <= points[2].getX() && y >= points[0].getY() && y <= points[2].getY());
	}

	public boolean InsidePoint2D(Point2D point) {
		Point[] points = this.GetPoints();
		return (point.getX() >= points[0].getX() && point.getX() <= points[2].getX() && point.getY() >= points[0].getY()
				&& point.getY() <= points[2].getY());
	}

	public boolean Contains(RectangleTrainee other) {
		Point[] points = this.GetPoints();
		Point[] pointsOther = other.GetPoints();
		
		return (points[0].getX() <= pointsOther[0].getX() && points[0].getY() <= pointsOther[0].getY()
				&& points[2].getX() >= pointsOther[2].getX() && points[2].getY() >= pointsOther[2].getY());

	}

	public boolean Intersection(RectangleTrainee other) {
		
		if (this.Contains(other))
			return false;
		
		double xMin = 100500;
		double xMax = -100500;
		double yMin = 100500;
		double yMax = -100500;
		
		Point[] points = this.GetPoints();
		for (Point el : points){
			
			if (el.getX() < xMin){
				xMin = el.getX();
			}
			
			if (el.getX() > xMax){
				xMax = el.getX();
			}
			
			if (el.getY() < yMin){
				yMin = el.getY();
			}
			
			if (el.getY() > yMax){
				yMax = el.getY();
			}
		}
		
		points = other.GetPoints();
		
		for (Point el : points){
			if (el.getX() >= xMin && el.getX() <= xMax && el.getY() >= yMin && el.getY() <= yMax)
				return true;
		}
		
		return false;
	}

	public void Large(int n) throws ArithmeticException {
		
		if (n == 0) {
			throw new ArithmeticException("Error in input data: parameter is zero");
		}
		
		if (n < 0) {
			throw new ArithmeticException("Error in input data: parameter is negative");
		}
		
		Point[] points = this.GetPoints();
		double length12 = this.CalculateLength(points[0].getX(), points[0].getY(), points[1].getX(), points[1].getY());
		double length23 = this.CalculateLength(points[1].getX(), points[1].getY(), points[2].getX(), points[2].getY());
		double length34 = this.CalculateLength(points[2].getX(), points[2].getY(), points[3].getX(), points[3].getY());
		double length14 = this.CalculateLength(points[3].getX(), points[3].getY(), points[0].getX(), points[0].getY());

		ChangeCoordinates(0, - ((n - 1) * length14) / 2, - ((n - 1) * length12) / 2);
		ChangeCoordinates(1, - ((n - 1) * length23) / 2, ((n - 1) * length12) / 2);
		ChangeCoordinates(2, ((n - 1) * length23) / 2, ((n - 1) * length34) / 2);
		ChangeCoordinates(3, ((n - 1) * length14) / 2, - ((n - 1) * length34) / 2);
	}

	private double CalculateLength(double x1, double y1, double x2, double y2) {
		return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
	}

}
