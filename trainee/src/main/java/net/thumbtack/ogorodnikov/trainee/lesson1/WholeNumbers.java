package net.thumbtack.ogorodnikov.trainee.lesson1;

import java.util.Scanner;

public class WholeNumbers {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int a = in.nextInt();
		int b = in.nextInt();

		System.out.println("Sum: " + (a + b));
		System.out.println("Product: " + (a * b));
		System.out.println("Division: " + (double) a / b);
		System.out.println("Remainder: " + (a % b));

		if (a < b) {
			System.out.println("a less than b");
		} else {
			if (a > b) {
				System.out.println("b less than a");
			} else {
				System.out.println("a is equals b");
			}
		}

		in.close();
	}
}
